/* LVGL Example project
 *
 * Basic project to test LVGL on ESP32 based projects.
 *
 * This example code is in the Public Domain (or CC0 licensed, at your option.)
 *
 * Unless required by applicable law or agreed to in writing, this
 * software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_freertos_hooks.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "driver/gpio.h"


/* Littlevgl specific */
#ifdef LV_LVGL_H_INCLUDE_SIMPLE
#include "lvgl.h"
#else
#include "lvgl/lvgl.h"
#endif

#include "lvgl_helpers.h"

/*
#ifndef CONFIG_LV_TFT_DISPLAY_MONOCHROME
    #if defined CONFIG_LV_USE_DEMO_WIDGETS
        #include "lv_examples/src/lv_demo_widgets/lv_demo_widgets.h"
    #elif defined CONFIG_LV_USE_DEMO_KEYPAD_AND_ENCODER
        #include "lv_examples/src/lv_demo_keypad_encoder/lv_demo_keypad_encoder.h"
    #elif defined CONFIG_LV_USE_DEMO_BENCHMARK
        #include "lv_examples/src/lv_demo_benchmark/lv_demo_benchmark.h"
    #elif defined CONFIG_LV_USE_DEMO_STRESS
        #include "lv_examples/src/lv_demo_stress/lv_demo_stress.h"
    #else
        #error "No demo application selected."
    #endif
#endif
*/

/*********************
 *      DEFINES
 *********************/
#define TAG "demo"
#define LV_TICK_PERIOD_MS 1


#define RETRY_COUNT 10

#define cont_x 480
#define cont_y 320

#define bar_width 280
#define bar_height 30

#define bar_pos_x 160
#define bar_pos_y 270

#define value_coord_x 200
#define value_coord_y 100

#define value_size_x 200
#define value_size_y 100

#define color_bar_width 15


#define title_x 120
#define title_y 20


#define green_bar_pos_x 160
#define green_bar_pos_y 257


#define green_bar_length 280
#define green_bar_width 15

#define zero_indicator_x 150
#define zero_indicator_y 220

/*********************
 * IMAGE DECLARATIONS
*********************/

LV_IMG_DECLARE(earthbondsymbol);
LV_IMG_DECLARE(secondclass);
LV_IMG_DECLARE(logo);

/*********************
 * FONT DECLARATIONS
*********************/
LV_FONT_DECLARE(montserrat_medium_72);

LV_FONT_DECLARE(montserrat_medium_60);
/**********************
*  STATIC PROTOTYPES
**********************/


void myTickRoutine();
static void lv_tick_task(void *arg);
static void guiTask(void *pvParameter);


static void standby();
static void undefined();
static void earth_bond();
static void earth_leakage();
static void load_test_low();
static void load_test_high();
static void cl1_flash();
static void cl2_flash();
static void calibration();

void tabs();

/**********************
*  CONTAINER DECLARATION
**********************/

lv_obj_t * undef_cont;
lv_obj_t * standby_cont;
lv_obj_t * earth_bond_cont;
lv_obj_t * earth_leak_cont;
lv_obj_t * load_test_low_cont;
lv_obj_t * load_test_high_cont;
lv_obj_t * pass_flash_cont;
lv_obj_t * tip_cont;
lv_obj_t * cl2_pass_flash_cont;



//STYLES
static lv_style_t style_window;
static lv_style_t style_digit_text;
//BAR
static lv_style_t style_bar;
static lv_style_t style_indic;
//Indicator Styling
static lv_style_t square;
//Style Green bar
static lv_style_t green_bar_btn;
//Style display
static lv_style_t disp_val_button;
static lv_style_t digit_style;
static lv_style_t style_indicator;
static lv_style_t style_title_text;
static lv_style_t undefined_font;


//Earth Bond
static lv_obj_t * earth_bond_bar;
static lv_obj_t * eb_label;


//EarthLeakage
static lv_obj_t * earth_leak_bar;
static lv_obj_t * el_label;


//load test low
static lv_obj_t * low_load_test_bar;
static lv_obj_t * llt_label;


//load test high
static lv_obj_t * load_test_bar;
static lv_obj_t * lt_label;


//Class 1 flash
static lv_obj_t * pass_flash_test_title;
static lv_obj_t * pass_btn;
static lv_obj_t * pass_amp_label;
static lv_obj_t * label;
static lv_obj_t * pass_amp_label1;
static lv_obj_t * hold;

//Class1 styles
static lv_style_t tab2_pass_btn;
static lv_style_t tab3_waiting_btn;
static lv_style_t tab3_fail_btn;


//class2 flash
static lv_obj_t * cl2_pass_flash_test_title;
static lv_obj_t * cl2_pass_btn;
static lv_obj_t * cl2_label;

static lv_obj_t * cl2_pass_amp_label;
static lv_obj_t * cl2_pass_amp_label1;
static lv_obj_t * cl2_hold;

//class 2 styles
static lv_style_t cl2_tab2_pass_btn;
static lv_style_t cl2_tab3_waiting_btn;
static lv_style_t cl2_tab3_fail_btn;

//calibration
static lv_obj_t * cal;




/**********************
 *   GLOBAL VARIABLES
 **********************/
int mode = 0;
float value = 0;

/**********************
 *   APPLICATION MAIN
 **********************/
void app_main() {

    /* If you want to use a task to create the graphic, you NEED to create a Pinned task
     * Otherwise there can be problem such as memory corruption and so on.
     * NOTE: When not using Wi-Fi nor Bluetooth you can pin the guiTask to core 0 */
    xTaskCreatePinnedToCore(guiTask, "gui", 4096*4, NULL, 0, NULL, 1);
}



/* Creates a semaphore to handle concurrent call to lvgl stuff
 * If you wish to call *any* lvgl function from other threads/tasks
 * you should lock on the very same semaphore! */
SemaphoreHandle_t xGuiSemaphore;

//place screens into this function

static void guiTask(void *pvParameter) {

    (void) pvParameter;
    xGuiSemaphore = xSemaphoreCreateMutex();

    lv_init();

    /* Initialize SPI or I2C bus used by the drivers */
    lvgl_driver_init();

    lv_color_t* buf1 = heap_caps_malloc(DISP_BUF_SIZE * sizeof(lv_color_t), MALLOC_CAP_DMA);
    assert(buf1 != NULL);

    /* Use double buffered when not working with monochrome displays */
#ifndef CONFIG_LV_TFT_DISPLAY_MONOCHROME
    lv_color_t* buf2 = heap_caps_malloc(DISP_BUF_SIZE * sizeof(lv_color_t), MALLOC_CAP_DMA);
    assert(buf2 != NULL);
#else
    static lv_color_t *buf2 = NULL;
#endif

    static lv_disp_buf_t disp_buf;

    uint32_t size_in_px = DISP_BUF_SIZE;

#if defined CONFIG_LV_TFT_DISPLAY_CONTROLLER_IL3820         \
    || defined CONFIG_LV_TFT_DISPLAY_CONTROLLER_JD79653A    \
    || defined CONFIG_LV_TFT_DISPLAY_CONTROLLER_UC8151D     \
    || defined CONFIG_LV_TFT_DISPLAY_CONTROLLER_SSD1306

    /* Actual size in pixels, not bytes. */
    size_in_px *= 8;
#endif

    /* Initialize the working buffer depending on the selected display.
     * NOTE: buf2 == NULL when using monochrome displays. */
    lv_disp_buf_init(&disp_buf, buf1, buf2, size_in_px);

    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.flush_cb = disp_driver_flush;

    /* When using a monochrome display we need to register the callbacks:
     * - rounder_cb
     * - set_px_cb */
#ifdef CONFIG_LV_TFT_DISPLAY_MONOCHROME
    disp_drv.rounder_cb = disp_driver_rounder;
    disp_drv.set_px_cb = disp_driver_set_px;
#endif

    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);

    /* Register an input device when enabled on the menuconfig */
#if CONFIG_LV_TOUCH_CONTROLLER != TOUCH_CONTROLLER_NONE
    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);
    indev_drv.read_cb = touch_driver_read;
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    lv_indev_drv_register(&indev_drv);
#endif

    /* Create and start a periodic timer interrupt to call lv_tick_inc */
    const esp_timer_create_args_t periodic_timer_args = {
        .callback = &lv_tick_task,
        .name = "periodic_gui"
    };
    esp_timer_handle_t periodic_timer;
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, LV_TICK_PERIOD_MS * 1000));

    // Create the demo application
    //create_demo_application();

    ///Style Declarations used for all the containers
    //styles used in all containers
    //Windows
	lv_style_init(&style_window);
	lv_style_set_bg_color(&style_window,LV_STATE_DEFAULT,LV_COLOR_WHITE);
	lv_style_set_bg_color(&style_window,LV_STATE_DEFAULT,lv_color_hex(0xE6E6E6));
	lv_style_init(&style_digit_text);
	lv_style_set_text_font(&style_digit_text, LV_STATE_DEFAULT,&montserrat_medium_72);


	//BAR STYLING

	lv_style_set_bg_color(&style_bar,LV_STATE_DEFAULT,LV_COLOR_WHITE);
	//lv_style_set_bg_color(&style_bar,LV_STATE_DEFAULT,lv_color_hex(0xCBCBCB);
	lv_style_set_radius(&style_bar, LV_STATE_DEFAULT, 0);
	lv_style_set_border_color(&style_bar,LV_STATE_DEFAULT,LV_COLOR_BLACK);
	lv_style_set_border_width(&style_bar, LV_STATE_DEFAULT, 3);
	lv_style_set_border_side(&style_bar,LV_STATE_DEFAULT,LV_BORDER_SIDE_BOTTOM|LV_BORDER_SIDE_RIGHT|LV_BORDER_SIDE_LEFT);

	//ACTIVE BAR PART
	lv_style_init(&style_indic);
	lv_style_set_bg_color(&style_indic,LV_STATE_DEFAULT,LV_COLOR_SILVER);
	lv_style_set_radius(&style_indic, LV_STATE_DEFAULT, 7);
	lv_style_set_border_color(&style_indic,LV_STATE_DEFAULT,LV_COLOR_BLACK);
	lv_style_set_border_width(&style_indic, LV_STATE_DEFAULT, 2);

	//lv_style_set_border_side(&style_indic,LV_STATE_DEFAULT,LV_BORDER_SIDE_RIGHT);
	lv_style_set_pad_top(&style_indic,LV_BAR_PART_BG,15);

	//lv_style_set_pad_bottom(&style_indic,LV_STATE_DEFAULT,15);
	lv_style_set_border_side(&style_indic,LV_STATE_DEFAULT,LV_BORDER_SIDE_BOTTOM|LV_BORDER_SIDE_RIGHT|LV_BORDER_SIDE_LEFT|LV_BORDER_SIDE_TOP);


	//Indicator Styling
	lv_style_set_radius(&square, LV_STATE_DEFAULT, 0);
	lv_style_set_bg_opa(&square, LV_STATE_DEFAULT, LV_OPA_COVER);
	lv_style_set_bg_color(&square, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_grad_color(&square, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_grad_dir(&square, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
	lv_style_set_border_color(&square, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));


	//Style Green bar
	/*
	lv_style_init(&green_bar_btn);
	lv_style_set_radius(&green_bar_btn, LV_STATE_DEFAULT, 0);
	lv_style_set_bg_opa(&green_bar_btn, LV_STATE_DEFAULT, LV_OPA_COVER);
	lv_style_set_bg_color(&green_bar_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
	lv_style_set_bg_grad_color(&green_bar_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
	lv_style_set_bg_grad_dir(&green_bar_btn, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
	lv_style_set_border_color(&green_bar_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
	lv_style_set_border_width(&green_bar_btn, LV_STATE_DEFAULT, 2);

*/
	lv_style_init(&green_bar_btn);
	lv_style_set_radius(&green_bar_btn, LV_STATE_DEFAULT, 0);
	lv_style_set_bg_opa(&green_bar_btn, LV_STATE_DEFAULT, LV_OPA_COVER);
	lv_style_set_bg_color(&green_bar_btn, LV_STATE_DEFAULT, lv_color_hex(0xFF6F00));
	lv_style_set_bg_grad_color(&green_bar_btn, LV_STATE_DEFAULT, lv_color_hex(0xFF6F00));
	lv_style_set_bg_grad_dir(&green_bar_btn, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
	lv_style_set_border_color(&green_bar_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
	lv_style_set_border_width(&green_bar_btn, LV_STATE_DEFAULT, 2);
	//Style display
	lv_style_set_radius(&disp_val_button, LV_STATE_DEFAULT, 0);
	lv_style_set_bg_opa(&disp_val_button, LV_STATE_DEFAULT, LV_OPA_COVER);
	lv_style_set_bg_color(&disp_val_button, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_grad_color(&disp_val_button, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_grad_dir(&disp_val_button, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
	lv_style_set_border_color(&disp_val_button, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_color(&disp_val_button, LV_STATE_PRESSED, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_grad_color(&disp_val_button, LV_STATE_PRESSED, lv_color_hex(0xE6E6E6));



	lv_style_init(&digit_style);
	lv_style_set_bg_color(&digit_style, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_bg_grad_color(&disp_val_button, LV_STATE_PRESSED, lv_color_hex(0xE6E6E6));



	lv_style_init(&style_indicator);
	//lv_style_set_bg_color(&digit_style, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_text_font(&style_indicator, LV_STATE_DEFAULT,&lv_font_montserrat_20);


	lv_style_init(&style_digit_text);
	//lv_style_set_bg_color(&digit_style, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_text_font(&style_digit_text, LV_STATE_DEFAULT,&montserrat_medium_72);


	lv_style_init(&style_title_text);
	//lv_style_set_text_font(&style_title_text, LV_STATE_DEFAULT,&lv_font_montserrat_36);
	lv_style_set_text_font(&style_title_text, LV_STATE_DEFAULT,&lv_font_montserrat_40);

	lv_style_init(&undefined_font);
	lv_style_set_text_font(&undefined_font, LV_STATE_DEFAULT,&montserrat_medium_60);



	//class 1 styles
	lv_style_set_bg_color(&tab2_pass_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
	lv_style_set_radius(&tab2_pass_btn, LV_STATE_DEFAULT, 15);
	lv_style_set_border_color(&tab2_pass_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
	lv_style_set_border_opa(&tab2_pass_btn, LV_STATE_DEFAULT, LV_OPA_70);
	lv_style_set_border_width(&tab2_pass_btn, LV_STATE_DEFAULT, 3);


	lv_style_set_bg_color(&tab3_waiting_btn, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_radius(&tab3_waiting_btn, LV_STATE_DEFAULT, 15);
	lv_style_set_border_color(&tab3_waiting_btn, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_border_opa(&tab3_waiting_btn, LV_STATE_DEFAULT, LV_OPA_70);
	lv_style_set_border_width(&tab3_waiting_btn, LV_STATE_DEFAULT, 3);


	//Tab 3 fail button STYLE
	lv_style_set_border_color(&tab3_fail_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
	lv_style_set_border_opa(&tab3_fail_btn, LV_STATE_DEFAULT, LV_OPA_70);
	lv_style_set_border_width(&tab3_fail_btn, LV_STATE_DEFAULT, 3);
	lv_style_set_bg_color(&tab3_fail_btn, LV_STATE_DEFAULT, LV_COLOR_RED);
	lv_style_set_radius(&tab3_fail_btn, LV_STATE_DEFAULT, 15);


	//class 2
	lv_style_set_bg_color(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
	lv_style_set_radius(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, 15);
	lv_style_set_border_color(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
	lv_style_set_border_opa(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, LV_OPA_70);
	lv_style_set_border_width(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, 3);


	lv_style_set_bg_color(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_radius(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, 15);
	lv_style_set_border_color(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
	lv_style_set_border_opa(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, LV_OPA_70);
	lv_style_set_border_width(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, 3);

	//Tab 3 fail button border

	lv_style_set_border_color(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
	lv_style_set_border_opa(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, LV_OPA_70);
	lv_style_set_border_width(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, 3);
	lv_style_set_bg_color(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, LV_COLOR_RED);
	lv_style_set_radius(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, 15);




	standby();
	undefined();
	earth_bond();
	earth_leakage();
	load_test_low();

	load_test_high();
	cl1_flash();
	cl2_flash();
	calibration();
	tabs();


    /*A task should NEVER return */
    free(buf1);
#ifndef CONFIG_LV_TFT_DISPLAY_MONOCHROME
    free(buf2);
#endif
    vTaskDelete(NULL);
}

void myTickRoutine()
{
	/* Delay 1 tick (assumes FreeRTOS tick is 10ms */
	 vTaskDelay(pdMS_TO_TICKS(50));

	        /* Try to take the semaphore, call lvgl related function on success */
	 if (pdTRUE == xSemaphoreTake(xGuiSemaphore, portMAX_DELAY))
	 {
	     lv_task_handler();
	      xSemaphoreGive(xGuiSemaphore);
	 }

}

char buffer[20];


/*Random function to simulate data*/
float random_value()
{


	float x=rand()%25;
	return x;

}


int random_mode()
{

	return rand() %200;

}

int random_status()
{

	return rand() %16;
}






/*Tasks to control how screens and values change.*/
void screen_cb(lv_task_t *task2)
{
	mode=random_mode();
	mode=0;
	int status=random_status();

//	status = 0;
	value = random_value();
//	value=6;
	//value = 0.1;

	int tenfoldValue = value*10;


	switch(mode)
   	{
		case 0:
			//Undefined Mode
			lv_scr_load(undef_cont);
			printf("task: undefined container");

			break;
		case 1:
			//Standby
			lv_scr_load(standby_cont);
			printf("task: standby container");
			break;
		case 2:
			lv_scr_load(earth_bond_cont);

			//Value scaling and bar changing
			if(tenfoldValue<=1)
			{
					lv_bar_set_value(earth_bond_bar,(int)((100*value)/0.1),LV_ANIM_ON);
					lv_label_set_text_fmt(eb_label,"%.2f Ω\n",value);

					lv_label_set_align(eb_label,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(eb_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
			}

			else if(tenfoldValue>1 && tenfoldValue<=5)
			{
					lv_bar_set_value(earth_bond_bar,(int)((125*value)+87.5),LV_ANIM_ON);
					lv_label_set_text_fmt(eb_label,"%.2f Ω\n",value);

					lv_label_set_align(eb_label,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(eb_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
			}
			else if(tenfoldValue>5 && tenfoldValue<=10)
			{
					lv_bar_set_value(earth_bond_bar,(int)((50*value)+125),LV_ANIM_ON);
					lv_label_set_text_fmt(eb_label,"%.2f Ω\n",value);

					lv_label_set_align(eb_label,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(eb_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
			}
			else if(tenfoldValue>10 && tenfoldValue<=20)
			{
											//red range
				lv_bar_set_value(earth_bond_bar,(int)((25*value)+150),LV_ANIM_ON);
				lv_label_set_text_fmt(eb_label,"%.2f Ω\n",value);

				lv_label_set_align(eb_label,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(eb_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));

			}
			else
			{
				//greater than range
				lv_bar_set_value(earth_bond_bar,200,LV_ANIM_ON);
				lv_label_set_text_fmt(eb_label,">2 Ω\n");

				lv_label_set_align(eb_label,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(eb_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
			}
			break;

		case 3:
			lv_scr_load(earth_leak_cont);
			printf("task: Earth Leak container");

			//Value scaling and bar changing
			lv_bar_set_value(earth_leak_bar,(value*100),LV_ANIM_ON);
			lv_label_set_text_fmt(el_label,"%.2f mA\n",value);
			lv_label_set_align(el_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(el_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));

			break;
		case 4:
			lv_scr_load(load_test_low_cont);
			printf("task: Load test low container");

			//Value scaling and bar changing
			lv_bar_set_value(low_load_test_bar,(value*100),LV_ANIM_ON);
			lv_label_set_text_fmt(llt_label,"%.2f A\n",value);
			lv_label_set_align(llt_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(llt_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
			break;


		case 5:
			lv_scr_load(load_test_high_cont);
			printf("task: load test high container");

			//Value scaling and bar changing
			lv_bar_set_value(load_test_bar,(value*10),LV_ANIM_ON);
			lv_label_set_text_fmt(lt_label,"%.2f A\n",value);
			lv_label_set_align(lt_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(lt_label, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
			break;


		case 6:
			lv_scr_load(pass_flash_cont);
			printf("task: 500v Flash container");

			//Conditionally building flash containers

			lv_label_set_text_fmt(pass_flash_test_title, "500V LN-E Flash");
			lv_label_set_align(pass_flash_test_title,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(pass_flash_test_title, NULL, LV_ALIGN_IN_TOP_MID,30,(cont_y/10));





			lv_label_set_text_fmt(label, " ");
			lv_obj_add_style(pass_btn, LV_BTN_PART_MAIN, &tab3_waiting_btn);


			//hold or running bit 3 and bit 0 set
			lv_label_set_text_fmt(pass_amp_label, (status&2)? "Limit: 10mA":"Limit: 5mA");
			lv_label_set_align(pass_amp_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(pass_amp_label, NULL,LV_ALIGN_IN_TOP_MID,0,((62*cont_y)/100));

			//Presenting labels
			if(status&1)
			{
				if(status&4)
				{
							lv_label_set_text_fmt(pass_amp_label1, "In Progress");
							lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
							lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
							lv_label_set_text_fmt(pass_amp_label1, "Test Complete");
							lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
							lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{
						lv_label_set_text_fmt(pass_amp_label1, "Waiting");
						lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
						lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
			}

			if(status&9)
			{
					lv_label_set_text_fmt(label, (status&4) ? "#ffffff Pass " : "#ffffff Fail");
					lv_obj_add_style(pass_btn, LV_BTN_PART_MAIN,(status&4) ? &tab2_pass_btn: &tab3_fail_btn);

			}


			if(status&8)
			{	//lv_label_set_text_fmt(pass_amp_label1, "Test Complete");
				lv_label_set_text_fmt(hold, "Hold");
				lv_label_set_align(hold,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));

				if(status&4)
				{
					lv_label_set_text_fmt(pass_amp_label1, "In Progress");
					lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
					lv_label_set_text_fmt(pass_amp_label1, "Test Complete");
					lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{

				lv_label_set_text_fmt(hold, " ");
				lv_label_set_align(hold,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));

			}
			break;



		case 7:

			lv_scr_load(pass_flash_cont);

			lv_label_set_text_fmt(pass_flash_test_title, "1250V LN-E Flash");
			lv_label_set_align(pass_flash_test_title,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(pass_flash_test_title, NULL, LV_ALIGN_IN_TOP_MID,30,(cont_y/10));


			lv_label_set_text_fmt(label, " ");
			lv_obj_add_style(pass_btn, LV_BTN_PART_MAIN, &tab3_waiting_btn);


			//hold or running bit 3 and bit 0 set
			lv_label_set_text_fmt(pass_amp_label, (status&2)? "Limit: 10mA":"Limit: 5mA");
			lv_label_set_align(pass_amp_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(pass_amp_label, NULL,LV_ALIGN_IN_TOP_MID,0,((62*cont_y)/100));


			if(status&1)
			{

				if(status&4)
				{
					lv_label_set_text_fmt(pass_amp_label1, "In Progress");
					lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
					lv_label_set_text_fmt(pass_amp_label1, "Test Complete");
					lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{
			lv_label_set_text_fmt(pass_amp_label1, "Waiting");
			lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
			}

			if(status&9)
			{
			lv_label_set_text_fmt(label, (status&4) ? "#ffffff Pass " : "#ffffff Fail");
			lv_obj_add_style(pass_btn, LV_BTN_PART_MAIN,(status&4) ? &tab2_pass_btn: &tab3_fail_btn);
			}


			if(status&8)
			{
			lv_label_set_text_fmt(hold, "Hold");
			lv_label_set_align(hold,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));
				if(status&4)
				{
					lv_label_set_text_fmt(pass_amp_label1, "In Progress");
					lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
					lv_label_set_text_fmt(pass_amp_label1, "Test Complete");
					lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{
			lv_label_set_text_fmt(hold, " ");
			lv_label_set_align(hold,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));
			}
			break;

		case 8:
			lv_scr_load(cl2_pass_flash_cont);
			printf("task: 3000V Flash container");



			lv_label_set_text_fmt(cl2_pass_flash_test_title, "3000V LN-E Flash");
			lv_label_set_align(cl2_pass_flash_test_title,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(cl2_pass_flash_test_title, NULL, LV_ALIGN_IN_TOP_MID,30,(cont_y/10));


			lv_label_set_text_fmt(cl2_label, " ");
			lv_obj_add_style(cl2_pass_btn, LV_BTN_PART_MAIN, &cl2_tab3_waiting_btn);


			//hold or running bit 3 and bit 0 set

			lv_label_set_text_fmt(cl2_pass_amp_label, (status&2)? "Limit: 10mA":"Limit: 5mA");
			lv_label_set_align(cl2_pass_amp_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(cl2_pass_amp_label, NULL,LV_ALIGN_IN_TOP_MID,0,((62*cont_y)/100));


			if(status&1)
			{

				if(status&4)
				{
					lv_label_set_text_fmt(cl2_pass_amp_label1, "In Progress");
					lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
					lv_label_set_text_fmt(cl2_pass_amp_label1, "Test Complete");
					lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{
				lv_label_set_text_fmt(cl2_pass_amp_label1, "Waiting");
				lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
			}

			if(status&9)
			{
				lv_label_set_text_fmt(cl2_label, (status&4) ? "#ffffff Pass " : "#ffffff Fail");
				lv_obj_add_style(cl2_pass_btn, LV_BTN_PART_MAIN,(status&4) ? &cl2_tab2_pass_btn: &cl2_tab3_fail_btn);
			}

			//checking the 4th bit
			if(status&8)
			{
				lv_label_set_text_fmt(cl2_hold, "Hold");
				lv_label_set_align(cl2_hold,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(cl2_hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));

				if(status&4)
				{
					lv_label_set_text_fmt(cl2_pass_amp_label1, "In Progress");
					lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
					lv_label_set_text_fmt(cl2_pass_amp_label1, "Test Complete");
					lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}

			}
			else
			{
				lv_label_set_text_fmt(cl2_hold, " ");
				lv_label_set_align(cl2_hold,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(cl2_hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));
			}

			break;
		case 9:

			lv_scr_load(cl2_pass_flash_cont);
			printf("task: 3750V Flash container");

			lv_label_set_text_fmt(cl2_pass_flash_test_title, "3750V LN-E Flash");
			lv_label_set_align(cl2_pass_flash_test_title,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(cl2_pass_flash_test_title, NULL, LV_ALIGN_IN_TOP_MID,30,(cont_y/10));


			lv_label_set_text_fmt(cl2_label, " ");
			lv_obj_add_style(cl2_pass_btn, LV_BTN_PART_MAIN, &cl2_tab3_waiting_btn);


			//hold or running bit 3 and bit 0 set
			lv_label_set_text_fmt(cl2_pass_amp_label, (status&2)? "Limit: 10mA":"Limit: 5mA");
			lv_label_set_align(cl2_pass_amp_label,LV_LABEL_ALIGN_CENTER);
			lv_obj_align(cl2_pass_amp_label, NULL,LV_ALIGN_IN_TOP_MID,0,((62*cont_y)/100));




			if(status&1)
			{

				if(status&4)
				{
						lv_label_set_text_fmt(cl2_pass_amp_label1, "In Progress");
						lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
						lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
						lv_label_set_text_fmt(cl2_pass_amp_label1, "Test Complete");
						lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
						lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{
				lv_label_set_text_fmt(cl2_pass_amp_label1, "Waiting");
				lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
			}

			if(status&9)
			{
				lv_label_set_text_fmt(cl2_label, (status&4) ? "#ffffff Pass " : "#ffffff Fail");
				lv_obj_add_style(cl2_pass_btn, LV_BTN_PART_MAIN,(status&4) ? &cl2_tab2_pass_btn: &cl2_tab3_fail_btn);
				}


				if(status&8)
				{
				lv_label_set_text_fmt(cl2_hold, "Hold");
				lv_label_set_align(cl2_hold,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(cl2_hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));

				if(status&4)
					{
					lv_label_set_text_fmt(cl2_pass_amp_label1, "In Progress");
					lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
				else
				{
					lv_label_set_text_fmt(cl2_pass_amp_label1, "Test Complete");
					lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
					lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));
				}
			}
			else
			{
				lv_label_set_text_fmt(cl2_hold, " ");
				lv_label_set_align(cl2_hold,LV_LABEL_ALIGN_CENTER);
				lv_obj_align(cl2_hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));
			}
			break;

		case 10:

			lv_scr_load(tip_cont);
			lv_label_set_text_fmt(cal,"%.2f\n",value);

			break;
		default:
			lv_scr_load(undef_cont);
			break;
   		}
}

void tabs()
{

	lv_task_t *task2 = lv_task_create(screen_cb,2500,LV_TASK_PRIO_MID,NULL);
	//lv_scr_load(undef_cont);

   while(1)
   {
   	   myTickRoutine();

   }

}

/*STANDBY CONTAINER CREATION*/
static void standby()
{
    standby_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(standby_cont, cont_x,cont_y);
    lv_obj_add_style(standby_cont, LV_BTN_PART_MAIN, &style_window);

    lv_obj_t *d255 = lv_label_create(standby_cont, NULL);
    lv_label_set_text(d255,"D255 IN\n");
    lv_obj_add_style(d255, LV_BTN_PART_MAIN, &style_digit_text);
    lv_obj_align(d255, NULL, LV_ALIGN_CENTER, 0,(-25));



    lv_obj_t *standby = lv_label_create(standby_cont, NULL);
    lv_label_set_text(standby,"STANDBY\n");
    lv_obj_add_style(standby, LV_BTN_PART_MAIN, &style_digit_text);
    lv_obj_align(standby, NULL, LV_ALIGN_CENTER, 0,45);



    lv_obj_t * img1 = lv_img_create(standby_cont, NULL);
    lv_img_set_src(img1, &logo);
    lv_obj_align(img1, NULL, LV_ALIGN_CENTER, 0, 100);

}

/*UNDEFINED CONTAINER CREATION*/
static void undefined()
{
		    //undefined container
		    undef_cont = lv_cont_create(NULL,NULL);
		    lv_obj_set_size(undef_cont, cont_x,cont_y);
		    lv_obj_add_style(undef_cont, LV_BTN_PART_MAIN, &style_window);
		    lv_obj_t *undef = lv_label_create(undef_cont, NULL);
		    //lv_obj_add_style(undef, LV_BTN_PART_MAIN, &style_digit_text);

		    lv_obj_add_style(undef, LV_BTN_PART_MAIN, &undefined_font);
		    lv_label_set_text(undef,"   Undefined  \n  state please \n choose a test.\n");
		    lv_obj_align(undef, NULL, LV_ALIGN_CENTER, 0,0);

}

/*EARTH BOND CONTAINER*/
static void earth_bond()
{
    // Earth Bond Test
    //Earth bond test container
    earth_bond_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(earth_bond_cont, cont_x,cont_y);
    lv_obj_add_style(earth_bond_cont, LV_BTN_PART_MAIN, &style_window);


// Earth Bond Title
    lv_obj_t * earth_bond_title = lv_label_create(earth_bond_cont, NULL);
    lv_obj_add_style(earth_bond_title, LV_LABEL_PART_MAIN, &style_title_text);
    lv_label_set_text(earth_bond_title, "Earth Bond");

    lv_label_set_align(earth_bond_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(earth_bond_title, NULL, LV_ALIGN_IN_TOP_MID,30,(cont_y/10));


//Class1 symbol
    lv_obj_t * earth_bond = lv_img_create(earth_bond_cont, NULL);
    lv_img_set_src(earth_bond, &earthbondsymbol);

    lv_label_set_align(earth_bond ,LV_LABEL_ALIGN_CENTER);
//    lv_obj_align(earth_bond, NULL, LV_ALIGN_IN_TOP_MID,(-(cont_x/5)),(cont_y/10));
    lv_obj_align(earth_bond, NULL, LV_ALIGN_IN_TOP_LEFT,(cont_x/5),(cont_y/10));
    lv_obj_move_foreground(earth_bond);


//Measurement bar
    earth_bond_bar = lv_bar_create(earth_bond_cont, NULL);
    lv_obj_add_style(earth_bond_bar, LV_BTN_PART_MAIN, &style_bar);
    lv_obj_add_style(earth_bond_bar,LV_BAR_PART_INDIC,&style_indic);
    lv_bar_set_range(earth_bond_bar,0,200);


    lv_obj_set_pos(earth_bond_bar, bar_pos_x, bar_pos_y);
    lv_obj_set_size(earth_bond_bar, (2*cont_x/3), bar_height);

    lv_label_set_align(earth_bond_bar,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(earth_bond_bar, NULL, LV_ALIGN_IN_TOP_MID,0,(3*cont_y/4));




//Value Label
    eb_label = lv_label_create(earth_bond_cont, NULL);
    lv_obj_add_style(eb_label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_text(eb_label, "- - - -");
    lv_label_set_align(eb_label,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(eb_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));



    /*Visual Indiciations*/
    /*Green button sectionstyle*/
    /*Green button*/
    static lv_style_t style_btn;
    lv_style_init(&style_btn);
    lv_style_set_radius(&style_btn, LV_STATE_DEFAULT, 0);
    lv_style_set_bg_opa(&style_btn, LV_STATE_DEFAULT, LV_OPA_COVER);
    lv_style_set_bg_color(&style_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
    lv_style_set_bg_grad_color(&style_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
    lv_style_set_bg_grad_dir(&style_btn, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
    lv_style_set_border_color(&style_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_style_set_border_width(&style_btn, LV_STATE_DEFAULT, 3);
    lv_style_set_border_side(&style_btn,LV_STATE_DEFAULT,LV_BORDER_SIDE_BOTTOM|LV_BORDER_SIDE_RIGHT|LV_BORDER_SIDE_TOP|LV_BORDER_SIDE_LEFT);


//Green  Bar
    lv_obj_t * btn = lv_btn_create(earth_bond_cont, NULL);
    lv_obj_add_style(btn, LV_BTN_PART_MAIN, &style_btn);

    lv_obj_set_pos(btn, (cont_x/6), ((3*cont_y/4)-15));
    lv_obj_set_size(btn, (cont_x/3), color_bar_width);

    /*Amber style button*/
    static lv_style_t amber_style_btn;
    lv_style_init(&amber_style_btn);
    lv_style_set_radius(&amber_style_btn, LV_STATE_DEFAULT, 0);
    lv_style_set_bg_opa(&amber_style_btn, LV_STATE_DEFAULT, LV_OPA_COVER);
    lv_style_set_bg_color(&amber_style_btn, LV_STATE_DEFAULT, lv_color_hex(0xEDB44A));
    lv_style_set_bg_grad_color(&amber_style_btn, LV_STATE_DEFAULT, lv_color_hex(0xEDB44A));
    lv_style_set_bg_grad_dir(&amber_style_btn, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
    lv_style_set_border_color(&amber_style_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_style_set_border_width(&amber_style_btn, LV_STATE_DEFAULT, 3);

    lv_style_set_border_side(&amber_style_btn,LV_STATE_DEFAULT,LV_BORDER_SIDE_BOTTOM|LV_BORDER_SIDE_RIGHT|LV_BORDER_SIDE_TOP);
    //amber button creation
    lv_obj_t * amber_btn = lv_btn_create(earth_bond_cont, NULL);
    lv_obj_add_style(amber_btn, LV_BTN_PART_MAIN, &amber_style_btn);
    //lv_obj_set_pos(amber_btn, (cont_x/2), green_bar_pos_y);
    lv_obj_set_pos(amber_btn, (cont_x/2), ((3*cont_y/4)-15));
    lv_obj_set_size(amber_btn, (((cont_x*24)/100)+5), color_bar_width);
    //lv_obj_set_size(amber_btn, 200, color_bar_width);

    //Red style button
    static lv_style_t red_style_btn;
    lv_style_init(&red_style_btn);
    lv_style_set_radius(&red_style_btn, LV_STATE_DEFAULT, 0);
    lv_style_set_bg_opa(&red_style_btn, LV_STATE_DEFAULT, LV_OPA_COVER);
    lv_style_set_bg_color(&red_style_btn, LV_STATE_DEFAULT, LV_COLOR_RED);
    lv_style_set_bg_grad_color(&red_style_btn, LV_STATE_DEFAULT, LV_COLOR_RED);
    lv_style_set_bg_grad_dir(&red_style_btn, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
    lv_style_set_border_color(&red_style_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_style_set_border_width(&red_style_btn, LV_STATE_DEFAULT, 3);


/*
    lv_style_set_border_width(&red_style_btn, LV_STATE_DEFAULT, 3);
*/
    lv_style_set_border_side(&red_style_btn,LV_STATE_DEFAULT,LV_BORDER_SIDE_BOTTOM|LV_BORDER_SIDE_RIGHT|LV_BORDER_SIDE_TOP);

    //red button creation
    lv_obj_t * red_btn = lv_btn_create(earth_bond_cont, NULL);
    //lv_obj_set_pos(red_btn, green_bar_pos_x+240, green_bar_pos_y);
    lv_obj_set_pos(red_btn,((cont_x/2)+((cont_x*24)/100)+5) , ((3*cont_y/4)-15));


    lv_obj_set_size(red_btn, (((9*cont_x)/100)-3) ,color_bar_width);
    lv_obj_add_style(red_btn, LV_BTN_PART_MAIN, &red_style_btn);



    lv_obj_t * label_thirty = lv_label_create(earth_bond_cont, NULL);
    lv_obj_set_pos(label_thirty, green_bar_pos_x, zero_indicator_y);
    //lv_obj_add_style(label_thirty, LV_BTN_PART_MAIN, &square);
    lv_obj_set_pos(label_thirty, ((cont_x/6)-10), (((3*cont_y/4)-40)));
    lv_obj_add_style(label_thirty, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(label_thirty, "0.0");




    lv_obj_t * label_sixty = lv_label_create(earth_bond_cont, NULL);
    //lv_obj_set_pos(label_sixty, (green_bar_pos_x+120), zero_indicator_y);
    lv_obj_set_pos(label_sixty, ((cont_x/2)-10), (((3*cont_y/4)-40)));
    //lv_obj_add_style(label_sixty, LV_BTN_PART_MAIN, &square);
    lv_obj_add_style(label_sixty, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(label_sixty, "0.1");



    lv_obj_t * label_hund = lv_label_create(earth_bond_cont, NULL);
    lv_obj_set_pos(label_hund, (((cont_x/2)+((cont_x*24)/100)-10)), (((3*cont_y/4)-40)));

    //lv_obj_add_style(label_hund, LV_BTN_PART_MAIN, &square);
    lv_obj_add_style(label_hund, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(label_hund, "1.0");



    /*2 value*/
    lv_obj_t * label_zero = lv_label_create(earth_bond_cont, NULL);
    lv_obj_set_pos(label_zero, ((((cont_x*80)/100))), (((3*cont_y/4)-40)));

    //lv_obj_add_style(label_hund, LV_BTN_PART_MAIN, &square);
    lv_obj_add_style(label_zero, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(label_zero, "2.0");
}

/*EARTH LEAKAGE*/
static void earth_leakage()
{


//Earth Leakage container

    earth_leak_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(earth_leak_cont, cont_x,cont_y);
    lv_obj_add_style(earth_leak_cont, LV_BTN_PART_MAIN, &style_window);



//Earth Leakage Title
    lv_obj_t * el_load_test_title = lv_label_create(earth_leak_cont, NULL);
    lv_obj_add_style(el_load_test_title, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(el_load_test_title, "Earth Leakage");

    lv_label_set_align(el_load_test_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(el_load_test_title, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/10));


    //Earth leak bar
    earth_leak_bar = lv_bar_create(earth_leak_cont, NULL);
    lv_obj_add_style(earth_leak_bar, LV_BTN_PART_MAIN, &style_bar);
    lv_obj_add_style(earth_leak_bar,LV_BAR_PART_INDIC,&style_indic);
    lv_bar_set_range(earth_leak_bar,0,500);

    lv_obj_set_pos(earth_leak_bar, bar_pos_x, bar_pos_y);
    lv_obj_set_size(earth_leak_bar, (2*cont_x/3), bar_height);

    lv_label_set_align(earth_leak_bar,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(earth_leak_bar, NULL, LV_ALIGN_IN_TOP_MID,0,(3*cont_y/4));



    //displaying current measurement values
    el_label = lv_label_create(earth_leak_cont, NULL);

    lv_obj_add_style(el_label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_text(el_label, "- - - -");

    lv_label_set_align(el_label,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(el_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));


    //Green Indicator
    lv_obj_t * el_meas_bar_btn = lv_btn_create(earth_leak_cont, NULL);
    lv_obj_add_style(el_meas_bar_btn, LV_BTN_PART_MAIN, &green_bar_btn);


    lv_obj_set_pos(el_meas_bar_btn, bar_pos_x, (bar_pos_y-color_bar_width));
    lv_obj_set_size(el_meas_bar_btn, (2*cont_x/3), color_bar_width);


    lv_label_set_align(el_meas_bar_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(el_meas_bar_btn, NULL, LV_ALIGN_IN_TOP_MID,0,((3*cont_y/4)-15));


    //Indicators
    //0 AMP
    lv_obj_t * el_label_zero_a = lv_label_create(earth_leak_cont, NULL);
    lv_obj_set_pos(el_label_zero_a, ((2*cont_x/10)-20), ((3*cont_y/4)-40));
    lv_obj_add_style(el_label_zero_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(el_label_zero_a, "0");


    //2.5 AMP BUTTON*
    lv_obj_t * el_label_five_a = lv_label_create(earth_leak_cont, NULL);          //Add a label to the butto
    //lv_obj_add_style(el_label_five_a, LV_BTN_PART_MAIN, &square);
    lv_obj_add_style(el_label_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(el_label_five_a, (((5*cont_x)/10)-20), ((3*cont_y/4)-40));
    lv_label_set_text(el_label_five_a, "2.5");


    //5 AMP BUTTON
    lv_obj_t * el_label_twn_five_a = lv_label_create(earth_leak_cont, NULL);          //Add a label to the button
    lv_obj_add_style(el_label_twn_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(el_label_twn_five_a, (((8*cont_x)/10) +3), ((3*cont_y/4)-40));                      //Set its position
    lv_label_set_text(el_label_twn_five_a, "5");

}

/*LOAD LOW */
static void load_test_low()
{
  /*low load test container*/

    load_test_low_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(load_test_low_cont, cont_x,cont_y);
    lv_obj_add_style(load_test_low_cont, LV_BTN_PART_MAIN, &style_window);


    //Title
    lv_obj_t * low_load_test_title = lv_label_create(load_test_low_cont, NULL);
    lv_obj_add_style(low_load_test_title, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(low_load_test_title, "Load Test Low");

    lv_label_set_align(low_load_test_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(low_load_test_title, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/10));



    //Measurement bar
    low_load_test_bar = lv_bar_create(load_test_low_cont, NULL);
    lv_obj_add_style(low_load_test_bar,LV_BAR_PART_INDIC,&style_indic);
    lv_obj_add_style(low_load_test_bar, LV_BTN_PART_MAIN, &style_bar);
    lv_bar_set_range(low_load_test_bar,0,250);



    lv_obj_set_pos(low_load_test_bar, bar_pos_x, bar_pos_y);
    lv_obj_set_size(low_load_test_bar, (2*cont_x/3), bar_height);

    lv_label_set_align(low_load_test_bar,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(low_load_test_bar, NULL, LV_ALIGN_IN_TOP_MID,0,(3*cont_y/4));



    //Display buttons
    llt_label = lv_label_create(load_test_low_cont, NULL);
    lv_obj_add_style(llt_label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_text(llt_label, "- - - -");

    lv_label_set_align(llt_label,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(llt_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));



    //Green Bar
    lv_obj_t * low_meas_bar_btn = lv_btn_create(load_test_low_cont, NULL);
    lv_obj_add_style(low_meas_bar_btn, LV_BTN_PART_MAIN, &green_bar_btn);


    lv_obj_set_pos(low_meas_bar_btn, green_bar_pos_x, green_bar_pos_y);
    lv_obj_set_size(low_meas_bar_btn, (2*cont_x/3), color_bar_width);

    lv_label_set_align(low_meas_bar_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(low_meas_bar_btn, NULL, LV_ALIGN_IN_TOP_MID,0,((3*cont_y/4)-15));

    //lv_obj_add_style(low_meas_bar_btn, LV_BTN_PART_MAIN, &green_bar_btn);


//Number Indicators
    //zero indicator

    lv_obj_t * low_label_zero_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_set_pos(low_label_zero_a,((20*cont_x/100)-20), ((3*cont_y/4)-40));
    lv_obj_add_style(low_label_zero_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(low_label_zero_a, "0");



    //0.5 AMP INDICATOR
    lv_obj_t * low_label_five_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_set_pos(low_label_five_a, ((32*cont_x/100)-25), ((3*cont_y/4)-40));
    lv_obj_add_style(low_label_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(low_label_five_a, "0.5");


    //1AMP INDICATOR

    lv_obj_t * label_one_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_set_pos(label_one_a, (((44*cont_x)/100)-20), ((3*cont_y/4)-40));
    lv_obj_add_style(label_one_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(label_one_a, "1.0");


    //1.5 AMP INDICATOR
    lv_obj_t * label_one_five_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_add_style(label_one_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(label_one_five_a, (((56*cont_x)/100)-10), ((3*cont_y/4)-40));
    lv_label_set_text(label_one_five_a, "1.5");



    //2 AMP INDICATOR
    lv_obj_t * label_two_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_add_style(label_two_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(label_two_a, (((68*cont_x)/100)-10), ((3*cont_y/4)-40));
    lv_label_set_text(label_two_a, "2.0");

    //2.5 AMP INDICATOR
    lv_obj_t * low_label_twn_five_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_add_style(low_label_twn_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(low_label_twn_five_a, (((8*cont_x)/10)), ((3*cont_y/4)-40));
    lv_label_set_text(low_label_twn_five_a , "2.5");


}


static void load_test_high()
{
//Load test container High

    load_test_high_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(load_test_high_cont, cont_x,cont_y);
    lv_obj_add_style(load_test_high_cont, LV_BTN_PART_MAIN, &style_window);


     //Title
    lv_obj_t * load_test_title = lv_label_create(load_test_high_cont, NULL);
    lv_obj_add_style(load_test_title, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(load_test_title, "Load Test High");

    lv_label_set_align(load_test_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(load_test_title, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/10));


    //Measurement bar
    load_test_bar = lv_bar_create(load_test_high_cont, NULL);
    lv_obj_add_style(load_test_bar, LV_BTN_PART_MAIN, &style_bar);
    lv_obj_add_style(load_test_bar,LV_BAR_PART_INDIC,&style_indic);
    lv_bar_set_range(load_test_bar,0,250);

    lv_obj_set_pos(load_test_bar, bar_pos_x, bar_pos_y);
    lv_obj_set_size(load_test_bar,  (2*cont_x/3), bar_height);

    lv_label_set_align(load_test_bar,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(load_test_bar, NULL, LV_ALIGN_IN_TOP_MID,0,(3*cont_y/4));


    //Actual value change display button
    //Display buttons


    lt_label = lv_label_create(load_test_high_cont, NULL);
    lv_obj_add_style(lt_label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_text(lt_label, "- - - -");

    lv_label_set_align(lt_label,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(lt_label, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));


    //Green Bar

    lv_obj_t * meas_bar_btn = lv_btn_create(load_test_high_cont, NULL);
    lv_obj_add_style(meas_bar_btn, LV_BTN_PART_MAIN, &green_bar_btn);

    lv_obj_set_pos(meas_bar_btn, green_bar_pos_x, green_bar_pos_y);
    lv_obj_set_size(meas_bar_btn, (2*cont_x/3), color_bar_width);

    lv_label_set_align(meas_bar_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(meas_bar_btn, NULL, LV_ALIGN_IN_TOP_MID,0,((3*cont_y/4)-15));




    /*lv_obj_t * low_label_zero_a = lv_label_create(load_test_low_cont, NULL);
    lv_obj_set_pos(low_label_zero_a,((20*cont_x/100)-20), ((3*cont_y/4)-40));
    lv_obj_add_style(low_label_zero_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(low_label_zero_a, "0");
     *
     * */


    //Zero Amp Indicators

    lv_obj_t * label_zero_a = lv_label_create(load_test_high_cont, NULL);
    lv_obj_set_pos(label_zero_a, ((20*cont_x/100)-20), ((3*cont_y/4)-40));


    lv_obj_add_style(label_zero_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_label_set_text(label_zero_a, "0");


    //FIVE AMP INDICATOR

    lv_obj_t * label_five_a = lv_label_create(load_test_high_cont, NULL);
    lv_obj_add_style(label_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(label_five_a, ((32*cont_x/100)-20), ((3*cont_y/4)-40));
    lv_label_set_text(label_five_a, "5");

    //TEN AMP INDICATOR


    lv_obj_t * label_ten_a = lv_label_create(load_test_high_cont, NULL);
    lv_label_set_text(label_ten_a, "10");
    lv_obj_add_style(label_ten_a, LV_BTN_PART_MAIN, &style_indicator);

    lv_obj_set_pos(label_ten_a, ((44*cont_x/100)-20), ((3*cont_y/4)-40));




    //15 AMP INDICATOR
    lv_obj_t * label_fifteen_a = lv_label_create(load_test_high_cont, NULL);
    lv_obj_add_style(label_fifteen_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(label_fifteen_a, ((56*cont_x/100)-10), ((3*cont_y/4)-40));
    lv_label_set_text(label_fifteen_a, "15");

    //20 AMP INDICATOR

    lv_obj_t * label_twenty_a = lv_label_create(load_test_high_cont, NULL);
    lv_obj_add_style(label_twenty_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(label_twenty_a, ((68*cont_x/100)-10), ((3*cont_y/4)-40));

    lv_label_set_text(label_twenty_a, "20");


    //25 AMP BUTTON

    lv_obj_t * label_twn_five_a = lv_label_create(load_test_high_cont, NULL);
    lv_obj_add_style(label_twn_five_a, LV_BTN_PART_MAIN, &style_indicator);
    lv_obj_set_pos(label_twn_five_a, ((80*cont_x/100)), ((3*cont_y/4)-40));

    lv_label_set_text(label_twn_five_a, "25");

}

/*500 1250 FLASH*/
static void cl1_flash()
{
	//Flash PASS container


    pass_flash_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(pass_flash_cont, cont_x,cont_y);
    lv_obj_add_style(pass_flash_cont, LV_BTN_PART_MAIN, &style_window);


    pass_flash_test_title = lv_label_create(pass_flash_cont, NULL);
    lv_obj_add_style(pass_flash_test_title, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(pass_flash_test_title, "Flash Test");


    lv_label_set_align(pass_flash_test_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(pass_flash_test_title, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/10));



    /*class 1 logo*/

    lv_obj_t * img3 = lv_img_create(pass_flash_cont, NULL);
    lv_img_set_src(img3, &earthbondsymbol);

    lv_label_set_align(img3 ,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(img3, NULL, LV_ALIGN_IN_TOP_LEFT,40,(cont_y/10));
    //lv_obj_align(img3, NULL, LV_ALIGN_IN_TOP_MID,(-(cont_x/3)),(cont_y/10));
    //lv_obj_align(img3, NULL, LV_ALIGN_CENTER, -170, -150);


    //Display button
    pass_btn = lv_btn_create(pass_flash_cont, NULL);
    lv_obj_set_size(pass_btn, 240, 100);

    lv_label_set_align(pass_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(pass_btn, NULL,LV_ALIGN_IN_TOP_MID,0,((3*cont_y)/10));

    lv_obj_add_style(pass_btn, LV_BTN_PART_MAIN, &tab3_waiting_btn);

    label = lv_label_create(pass_btn, NULL);
    lv_obj_add_style(label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_text(label, " ");
    lv_label_set_recolor(label, true);

//5/10ma

    pass_amp_label = lv_label_create(pass_flash_cont, NULL);
    lv_obj_add_style(pass_amp_label, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(pass_amp_label, "----");

    //lv_obj_set_pos(pass_amp_label, (value_coord_x), (value_coord_y+150));

    lv_label_set_align(pass_amp_label,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(pass_amp_label, NULL,LV_ALIGN_IN_TOP_MID,0,((62*cont_y)/100));

//Waiting or in progress
    pass_amp_label1 = lv_label_create(pass_flash_cont, NULL);
    lv_obj_add_style(pass_amp_label1, LV_BTN_PART_MAIN, &style_title_text);

    lv_label_set_text(pass_amp_label1, "----");
    //lv_obj_set_pos(hold, (value_coord_x), (value_coord_y+200));

    lv_label_set_align(pass_amp_label1,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));



//holding comments
    hold = lv_label_create(pass_flash_cont, NULL);
    lv_obj_add_style(hold, LV_BTN_PART_MAIN, &style_title_text);

    lv_label_set_text(hold, "----");

    //lv_obj_set_pos(hold, (value_coord_x), (value_coord_y+230));
    //lv_obj_set_pos(hold, 0, 0);
   lv_label_set_align(hold,LV_LABEL_ALIGN_CENTER);
   lv_obj_align(hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/100));


}

/*3000 3750 FLASH*/
static void cl2_flash()
{
//CLASS 2 FLASH CONTAINER
    cl2_pass_flash_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(cl2_pass_flash_cont, cont_x,cont_y);
    lv_obj_add_style(cl2_pass_flash_cont, LV_BTN_PART_MAIN, &style_window);


    cl2_pass_flash_test_title = lv_label_create(cl2_pass_flash_cont, NULL);
    lv_obj_add_style(cl2_pass_flash_test_title, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(cl2_pass_flash_test_title, "Flash Test");


    lv_label_set_align(pass_flash_test_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cl2_pass_flash_test_title, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/10));


    /*CLASS 2 IMAGE DISPLAY*/

    lv_obj_t * img7 = lv_img_create(cl2_pass_flash_cont, NULL);
    lv_img_set_src(img7, &secondclass);


    lv_label_set_align(img7 ,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(img7, NULL, LV_ALIGN_IN_TOP_LEFT,35,(cont_y/10));





    /*CLASS 2 & functions
    static lv_style_t cl2_tab2_pass_btn;
    lv_style_set_bg_color(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, lv_color_hex(0xA6EB52));
    lv_style_set_radius(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, 15);
    lv_style_set_border_color(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_style_set_border_opa(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, LV_OPA_70);
    lv_style_set_border_width(&cl2_tab2_pass_btn, LV_STATE_DEFAULT, 3);

    static lv_style_t cl2_tab3_waiting_btn;
    lv_style_set_bg_color(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
    lv_style_set_radius(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, 15);
    lv_style_set_border_color(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, lv_color_hex(0xE6E6E6));
    lv_style_set_border_opa(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, LV_OPA_70);
    lv_style_set_border_width(&cl2_tab3_waiting_btn, LV_STATE_DEFAULT, 3);

    //Tab 3 fail button border
    static lv_style_t cl2_tab3_fail_btn;
    lv_style_set_border_color(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    lv_style_set_border_opa(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, LV_OPA_70);
    lv_style_set_border_width(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, 3);
    lv_style_set_bg_color(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, LV_COLOR_RED);
    lv_style_set_radius(&cl2_tab3_fail_btn, LV_STATE_DEFAULT, 15);
    */


    /*
    //Display button
    lv_obj_t * pass_btn = lv_btn_create(pass_flash_cont, NULL);
    lv_obj_set_size(pass_btn, 240, 100);

    lv_label_set_align(pass_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(pass_btn, NULL,LV_ALIGN_IN_TOP_MID,0,((3*cont_y)/10));

    lv_obj_t * label = lv_label_create(pass_btn, NULL);
    lv_obj_add_style(label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_recolor(label, true);

    */




    //Creating pass button
    cl2_pass_btn = lv_btn_create(cl2_pass_flash_cont, NULL);
    lv_obj_set_size(cl2_pass_btn, 240, 100);

    lv_label_set_align(cl2_pass_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cl2_pass_btn, NULL,LV_ALIGN_IN_TOP_MID,0,((3*cont_y)/10));

    lv_label_set_align(cl2_pass_btn,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cl2_pass_btn, NULL,LV_ALIGN_IN_TOP_MID,0,((3*cont_y)/10));



    cl2_label = lv_label_create(cl2_pass_btn, NULL);
    lv_obj_add_style(cl2_label, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_recolor(cl2_label, true);







//5mA/10mA

    cl2_pass_amp_label = lv_label_create(cl2_pass_flash_cont, NULL);
    lv_obj_add_style(cl2_pass_amp_label, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(cl2_pass_amp_label, "- - - -");

    //lv_obj_set_pos(cl2_pass_amp_label, (value_coord_x), (value_coord_y+150));


    lv_label_set_align(cl2_pass_amp_label,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cl2_pass_amp_label, NULL,LV_ALIGN_IN_TOP_MID,0,((62*cont_y)/100));




//Waiting and in progress
    cl2_pass_amp_label1 = lv_label_create(cl2_pass_flash_cont, NULL);
    lv_obj_add_style(cl2_pass_amp_label1, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(cl2_pass_amp_label1, "- - - -");

    //lv_obj_set_pos(cl2_pass_amp_label1, (value_coord_x), (value_coord_y+200));


    lv_label_set_align(cl2_pass_amp_label1,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cl2_pass_amp_label1, NULL,LV_ALIGN_IN_TOP_MID,0,((74*cont_y)/100));



//holding comments
    cl2_hold = lv_label_create(cl2_pass_flash_cont, NULL);
    lv_obj_add_style(cl2_hold, LV_BTN_PART_MAIN, &style_title_text);
    lv_label_set_text(cl2_hold, "- - - -");

    //lv_obj_set_pos(cl2_hold, (value_coord_x), (value_coord_y+250));

    lv_label_set_align(cl2_hold,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cl2_hold, NULL,LV_ALIGN_IN_TOP_MID,0,((87*cont_y)/010));
}


static void calibration()
{

 /*Calibration window */
    tip_cont = lv_cont_create(NULL,NULL);
    lv_obj_set_size(tip_cont, cont_x,cont_y);
    lv_obj_add_style(tip_cont, LV_BTN_PART_MAIN, &style_window);


    lv_obj_t * cal_title = lv_label_create(tip_cont, NULL);
    lv_obj_add_style(cal_title, LV_LABEL_PART_MAIN, &style_title_text);
    lv_label_set_text(cal_title, "calibration");

    lv_label_set_align(cal_title,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cal_title, NULL, LV_ALIGN_IN_TOP_MID,0,(cont_y/10));

    //cal label

    cal = lv_label_create(tip_cont, NULL);
    lv_obj_add_style(cal, LV_BTN_PART_MAIN, &style_digit_text);
    lv_label_set_text(cal, "- - - -");
    lv_label_set_align(cal,LV_LABEL_ALIGN_CENTER);
    lv_obj_align(cal, NULL,LV_ALIGN_IN_TOP_MID,0,(cont_y/3));
}


static void lv_tick_task(void *arg) {
    (void) arg;

    lv_tick_inc(LV_TICK_PERIOD_MS);
}
